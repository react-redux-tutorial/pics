import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 
      'Client-ID 674cad64c93994896410f27270aa3083cbd717ad103fb69a24dcd350e88ee1c1'
  }
});